package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;



public class SortedCircularDoublyLinkedList<E> implements SortedList<E> {

	private Node<E> header;
	private int size;
	private Comparator cmp;
	
	public SortedCircularDoublyLinkedList() {
		this.header = new Node<E>();
		this.header.setNext(header);
		this.header.setPrevious(header);
		this.size = 0;
		cmp = new DefaultComparator<E>();
	}

	public SortedCircularDoublyLinkedList(Comparator comp) {
		this.header = new Node<E>();
		this.header.setNext(header);
		this.header.setPrevious(header);
		this.size = 0;
		cmp = new DefaultComparator<E>();
				
	}

	@Override
	public Iterator<E> iterator() {
		return new ForwardIterator();
	}

	//This method takes the respective object as a parameter and makes sure to properly identify 
	//the location where it is supposed to go in order to maintain an organized list
	@Override
	public boolean add(E obj) {
		if(this.isEmpty()) {
			Node<E> toAdd = new Node<E>(obj,this.header,this.header);
			this.header.setNext(toAdd);
			this.header.setPrevious(toAdd);
			this.size++;
		}
		else {
			if(cmp.compare(obj, header.getNext().getElement()) < 0) {
				Node<E> toAdd = new Node<E>(obj,header.getNext(),header);
				header.getNext().setPrevious(toAdd);
				header.setNext(toAdd);
				this.size++;
				return true;
			}
			else if(cmp.compare(obj, header.getPrevious().getElement()) > 0) {
				Node<E> toAdd = new Node<E>(obj,header,header.getPrevious());
				header.getPrevious().setNext(toAdd);
				header.setPrevious(toAdd);
				this.size++;
				return true;
			}
			else {
				Node<E> toTraverse = this.header.getNext();
				while(cmp.compare(obj, toTraverse.getElement()) > 0 && toTraverse.getElement() != null) {
					if((cmp.compare(obj, toTraverse.getElement())) == 0) {
						Node<E> toAdd = new Node<E>(obj,toTraverse.getNext(),toTraverse);
						toTraverse.getNext().setPrevious(toAdd);
						toTraverse.setNext(toAdd);
						this.size++;
						return true;
					}
					toTraverse = toTraverse.getNext();
				}
				toTraverse = toTraverse.getPrevious();
				Node<E> toAdd = new Node<E>(obj,toTraverse.getNext(),toTraverse);
				toTraverse.getNext().setPrevious(toAdd);
				toTraverse.setNext(toAdd);
				this.size++;
				return true;
			}
		}
		return true;
	}

	//returns list size
	@Override
	public int size() {
		return this.size;
	}

	//This method iterates through the list until it finds the object that it is supposed to remove
	//if the object is found it removes it, lowers the size and return true. If the object is not found
	//it then returns false
	@Override
	public boolean remove(E obj) {
		if(this.isEmpty()) {
			return false;
		}
		else {
			Node<E> toTraverse = this.header.getNext();
			while(toTraverse != this.header) {
				if(toTraverse.getElement() == obj) {
					toTraverse.getPrevious().setNext(toTraverse.getNext());
					toTraverse.getNext().setPrevious(toTraverse.getPrevious());
					toTraverse.clear();
					this.size--;
					return true;
				}
				toTraverse = toTraverse.getNext();
			}
		}
		return false;
	}

	//This methods iterates through the list until it finds the node at the parameter index.
	//It then proceeds to remove the node and lower its size. If the index were to be lower than
	//zero and higher than the list size then the method would return an IndexOutOfBounds exception
	@Override
	public boolean remove(int index) {
		if(this.isEmpty() || index > this.size()) {
			throw new IndexOutOfBoundsException();
		}
		else {
			int traverse = 0;
			Node<E> traveler = this.header.getNext();
			while(traverse != index) {
				traveler = traveler.getNext();
				traverse++;
			}
			traveler.getPrevious().setNext(traveler.getNext());
			traveler.getNext().setPrevious(traveler.getPrevious());
			traveler.clear();
			this.size--;
			return true;
		}
	}

	//This method is tasked with finding all of the different incarnations of the object parameter 
	//and both removes them and returns the amount of objects removed. If the parameter object is not
	//found then the method returns -1.
	@Override
	public int removeAll(E obj) {
		if(this.isEmpty()) {
			return 0;
		}
		else {
			int removed = 0;
			Node<E> toTraverse = this.header.getNext();
			while(toTraverse != this.header) {
				if(toTraverse.getElement() == obj) {
					this.remove(obj);
					removed++;
					toTraverse = this.header;
				}
				toTraverse = toTraverse.getNext();
			}
			return removed;
		}
	}


	//This method is in charge of returning the first element in the list
	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	//this element is in charge of returning the last element in the list
	@Override
	public E last() {
		return this.header.getPrevious().getElement();
	}


	//This method is tasked with finding the element at the parameter index. For that the method iterates
	//through the list until it finds the element at said index. If the index is less than zero and greater than\
	//the list size then the method throws and IndexOutOfBoundsException
	@Override
	public E get(int index) {
		if(this.isEmpty() || index > this.size()) {
			throw new IndexOutOfBoundsException();
		}
		else {
			int traverse = 0;
			Node<E> traveler = this.header.getNext();
			while(traverse != index) {
				traveler = traveler.getNext();
				traverse++;
			}
			return traveler.getElement();
		}
	}

	//This method is in charge of clearing the list. For that the method creates two Nodes, One to move across the list\
	//and another to actually preform the removal. Both nodes work together until the end of the list is reached.
	@Override
	public void clear() {
		Node<E> traverse = this.header.getNext();
		Node<E> remove = traverse;
		while(traverse != this.header) {
			traverse = traverse.getNext();
			remove.getPrevious().setNext(traverse.getNext());
			remove.getNext().setPrevious(traverse.getPrevious());
			remove.clear();
			remove = traverse;
		}
		this.size = 0;
	}

	//This method is in charge of finding out if the parameter object exists within the list. For
	//that the method iterates through the list until it finds the parameter object and then returns true.
	//If the parameter object is not found then the method returns false.
	@Override
	public boolean contains(E e) {
		if(this.isEmpty()) {
			return false;
		}
		else {
			Node<E> toTraverse = this.header.getNext();
			while(toTraverse != this.header) {
				if(toTraverse.getElement() == e) {
					return true;
				}
				toTraverse = toTraverse.getNext();
			}
			return false;
		}
	}

	//Method that returns true if the list is empty of false otherwise
	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}


	public Iterator<E> iterator(int index) {
		return new ForwardIndexIterator(index);
	}

	//This method is supposed to return the first index position where the object parameter is found. For that
	//it iterates through the list until it finds the parameter object and stores the amount of moves through the list
	//in a variable. If the object is not found then the method returns -1.
	@Override
	public int firstIndex(E e) {
		int index = 0;
		Node<E> traverse = this.header.getNext();
		while(traverse != this.header) {
			if(cmp.compare(traverse.getElement(),e) == 0) {
				return index;
			}
			traverse = traverse.getNext();
			index++;
		}
		return -1;
	}

	//This method is supposed to return the last index position where the object parameter is found. For that
	//it iterates through the full list and stores the position of the parameter object every time it is found.
	//It then returns the position of said parameter object. If the object is not found it returns -1.
	@Override
	public int lastIndex(E e) {
		int location = -1;
		int index = 0;
		Node<E> traverse = this.header.getNext();
		while(traverse != this.header) {
			if(cmp.compare(traverse.getElement(), e) == 0) {
				location = index;
			}
			traverse = traverse.getNext();
			index++;
		}
		return location;
	}


	public ReverseIterator<E> reverseIterator() {
		return new ReverseElementsIterator();
	}


	public ReverseIterator<E> reverseIterator(int index) {
		return new ReverseIndexIterator(index);
	}

	//Just a regular forward iterator
	private class ForwardIterator implements Iterator<E>{

		private Node<E> start;

		public ForwardIterator() {
			start = header.getNext();
		}

		@Override
		public boolean hasNext() {
			return start != header;
		}

		@Override
		public E next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			else {
				E toReturn = start.getElement();
				start = start.getNext();
				return toReturn;
			}
		}	
	}

	//This forward iterator takes advantage of the list's get method in order to return the specific index position. 
	//it uses an integer variable that is in charge of moving from the given parameter index all the way to the end of the list.
	private class ForwardIndexIterator implements Iterator<E>{

		private int start;

		public ForwardIndexIterator(int index) {
			start = index;
		}

		@Override
		public boolean hasNext() {
			return start != size();
		}

		@Override
		public E next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			else {
				E toReturn = get(start);
				start++;
				return toReturn;
			}
		}

	}

	//This is just a regular reverse iterator that takes advantage of the list's getPrevious Element to move
	//form start to finish
	private class ReverseElementsIterator implements ReverseIterator<E>{

		private Node<E> end;

		public ReverseElementsIterator() {
			end = header.getPrevious();
		}

		@Override
		public boolean hasPrevious() {
			return end != header;
		}

		@Override
		public E previous() {
			if(!hasPrevious()) {
				throw new NoSuchElementException();
			}
			else {
				E toReturn = end.getElement();
				end = end.getPrevious();
				return toReturn;
			}
		}

	}

	//This forward iterator takes advantage of the list's get method in order to return the specific index position. 
	//it uses an integer variable that is in charge of moving from the given parameter index all the way to the start of the list.
	private class ReverseIndexIterator implements ReverseIterator<E>{

		private int start;

		public ReverseIndexIterator(int index) {
			start = index;
		}


		@Override
		public boolean hasPrevious() {
			return start != 0;
		}

		@Override
		public E previous() {
			if(!hasPrevious()) {
				throw new NoSuchElementException();
			}
			else {
				E toReturn = get(start);
				start--;
				return toReturn;
			}
		}

	}

	private static class Node<T>{

		private T element;
		private Node<T> next;
		private Node<T> previous;

		public Node<T> getPrevious() {
			return previous;
		}

		public void setPrevious(Node<T> previous) {
			this.previous = previous;
		}

		public Node(){
			this.element = null;
			this.next = null;
			this.previous = null;
		}

		public Node(T e, Node<T> N, Node<T> P) {
			this.element = e;
			this.next = N;
			this.previous = P;
		}

		public T getElement() {
			return element;
		}

		public void setElement(T element) {
			this.element = element;
		}

		public Node<T> getNext() {
			return next;
		}

		public void setNext(Node<T> next) {
			this.next = next;
		}

		public void clear() {
			this.element = null;
			this.next = null;
			this.previous = null;
		}

		@Override
		public String toString() {
			return (String) this.element;
		}
	}

	public static class DefaultComparator<E> implements Comparator<E>{

		@Override
		public int compare(E o1, E o2) {
			try {
				return ((Comparable<E>)o1).compareTo(o2);
			} catch(ClassCastException e) {
				throw new IllegalArgumentException("Instantiated data type must be Comparator");
			}
		}

	}

	public E[] toArray(E[] arr) {
		ForwardIterator iter = new ForwardIterator();
		for(int i = 0; i < this.size(); i++) {
			arr[i] = iter.next();
		}
		return arr;
	}


}
