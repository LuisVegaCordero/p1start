package edu.uprm.cse.datastructures.cardealer.model;

import java.util.concurrent.CopyOnWriteArrayList;

import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDoublyLinkedList;

public class MockCarList {

	private static final CopyOnWriteArrayList<Car> cList = new CopyOnWriteArrayList<>();

	
	static {
		
		cList.add(new Car(1, "Toyota", "Yaris", "LE", 16000));
		cList.add(new Car(2, "Honda", "Fit", "S", 9000));
		cList.add(new Car(3, "Ford", "Fiesta", "SE", 6000));
		cList.add(new Car(3, "Jeep", "Cherokee", "S", 3500));
		
	}
	
	private MockCarList(){}
	  
	  public static CopyOnWriteArrayList<Car> getInstance(){
	    return cList;
	  }
	
	
}
